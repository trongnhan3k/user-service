package vn.cmc.global.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.cmc.global.base.BaseController;
import vn.cmc.global.service.UserService;
import vn.cmc.global.util.config.Constant;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = Constant.USER)
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    //get user
//    @GetMapping(Constant.USER_ID)
//    public ResponseEntity<?> findByUser(@PathVariable("userId") Long userId) {
//        if(userService.e)
//        return ResponseEntity.ok(userService.findByUserId(userId));
//    }

    //edit user
//    @PutMapping(Constant.EDIT)
//    public ResponseEntity<?> editUser(){
//
//    }

    //delete user
//    @DeleteMapping(Constant.DELETE)
//    public ResponseEntity<?> deleteUser(){
//
//    }
}
