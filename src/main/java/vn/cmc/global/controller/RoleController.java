package vn.cmc.global.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.cmc.global.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/role")
public class RoleController extends UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/hello")
    public ResponseEntity hello() {
        return new ResponseEntity<>("hello", HttpStatus.OK);
    }

}
