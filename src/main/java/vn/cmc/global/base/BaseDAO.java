package vn.cmc.global.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by nhanvt on 2020/06/09
 */
public abstract class BaseDAO extends Base {
    protected static final Logger dbLogger = LogManager.getLogger("DatabaseLog");

    protected void releaseResource(PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null) {
                ps.clearBatch();
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {

        }
    }

    protected void releaseConn(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            eLogger.error("ERROR closing conn ", e);
        }
    }

    protected abstract Connection getConnection(boolean autoCommit);

    protected abstract Connection getConnection();
}
