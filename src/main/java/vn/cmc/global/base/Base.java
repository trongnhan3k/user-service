package vn.cmc.global.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by nhanvt on 2020/06/09
 */
public abstract class Base {
    protected static final Logger eLogger = LogManager.getLogger("ErrorLog");
    protected static final Logger logger = LogManager.getLogger("CommonLog");
}
