package vn.cmc.global.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.cmc.global.util.global.GlobalObject;

/**
 * Created by nhanvt on 2020/06/09
 */
public abstract class BaseController extends Base {

    protected static final Logger logger = LoggerFactory.getLogger("RequestLog");

    protected String getResponse(Object baseReponse) {
        return GlobalObject.gsonNull.toJson(baseReponse);
    }
}
