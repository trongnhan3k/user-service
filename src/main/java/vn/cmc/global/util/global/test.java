package vn.cmc.global.util.global;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class test {
    public static void main(String[] args) {
        test test = new test();
        test.findMedianSortedArrays(new int[]{40, 11, 26, 27, -20});

        String[] arr = new String[]{
                "abdfsj,mghaslkdfg",
                "asdjkfhasdkjfh",
                "ahfdoibuawoefirhbalkfnbva",
                "asdfjhasdkf",
                "a"
        };

        String[] arrStr = {"abc", "abd", "aa", "zzzzz", "bcd", "aja", "abcdefgh", "1", "99999", "34534",
                "dfdfgd", "dfgdfgd", "dfgd33", "fdg456hjg", "fghfgh", "76", "34534", "fghfhfghfgfghf", "fghfg", "fghf456f", "fghfvc", "bnjkh", "dhsdfsd", "354fhf4363", "sdfsgsdgds"};

//        String[] out = doOutput(arrStr);
//        for (String s : out) {
//            System.out.println(s);
//        }
        solution(arrStr);
//        newSolution(arrStr);
    }

    public static void newSolution(String[] arr) {
        sortAlfabetical(arr);
        for (String s : arr) {
            System.out.println(s);
        }
    }

    public static void solution(String[] arr) {
        sort(arr, arr.length);
        for (String s : arr)
            System.out.println(s);
    }

    static void sort(String[] s, int n) {
        for (int i = 1; i < n; i++) {
            String temp = s[i];

            // Insert s[j] at its correct position
            int j = i - 1;
            while (j >= 0 && temp.length() < s[j].length()) {
                s[j + 1] = s[j];
                j--;
            }
            s[j + 1] = temp;
        }
    }

    public static void sortAlfabetical(String[] x) {
        int j;
        boolean found = true; // will determine when the sort is finished
        String temp;

        while (found) {
            found = false;
            for (j = 0; j < x.length - 1; j++) {
                if (x[j].compareToIgnoreCase(x[j + 1]) > 0) { // ascending sort
                    temp = x[j];
                    x[j] = x[j + 1]; // swap
                    x[j + 1] = temp;
                    found = true;
                }
            }
        }

    }


    public static void compare(String[] arrayOne) {

        Arrays.sort(arrayOne, Comparator.comparingInt(String::length));

        for (String s : arrayOne) {
            System.out.print(s + " ");
        }

    }

    public static String[] doOutput(String[] arrStr) {
        for (int i = 0; i < arrStr.length - 1; i++) {
            //String current = arrStr[i];
            for (int j = i + 1; j < arrStr.length; j++) {
                if (arrStr[i].length() > arrStr[j].length()) {
                    swap(arrStr, i, j);
                }
                if (arrStr[i].length() == arrStr[j].length()) {
                    for (int k = 0; k < arrStr[i].length(); k++) {
                        if (arrStr[i].charAt(k) > arrStr[j].charAt(k)) {
                            swap(arrStr, i, j);
                        }
                    }
                }
            }
        }
        return arrStr;
    }

    public static void swap(String[] arrStr, int i, int j) {
        String temp = arrStr[i];
        arrStr[i] = arrStr[j];
        arrStr[j] = temp;
    }

    public List<List<Integer>> findMedianSortedArrays(int[] arr) {
        sort(arr);
        int a = Math.abs(arr[1] - arr[0]);
        List<List<Integer>> rs = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            List<Integer> list = new ArrayList<>();
            int b = Math.abs(arr[i + 1] - arr[i]);
            if (b < a) {
                a = b;
                rs.clear();
                list.add(arr[i]);
                list.add(arr[i + 1]);
                rs.add(list);
            } else if (b == a) {
                list.add(arr[i]);
                list.add(arr[i + 1]);
                rs.add(list);
            }
        }

        return rs;
    }

    public static void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int tmp;
                if (arr[i] > arr[j]) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }
}
