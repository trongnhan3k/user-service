package vn.cmc.global.util.global;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by nhanvt on 2020/06/09
 */
public class GlobalObject {
    public static final Gson gsonNull = new GsonBuilder()
            .setPrettyPrinting()
            .serializeNulls()
//            .excludeFieldsWithoutExposeAnnotation()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();

    static {
    }
}
