package vn.cmc.global.util;

import org.springframework.stereotype.Component;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateUtils {
    public static final String patternDf = "yyyy-MM-dd";
    public static SimpleDateFormat df = new SimpleDateFormat(patternDf);

    public Date getDateFromString(String dateString) {
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Time convertTimeString(String time) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Time d = null;
        try {
            d = new Time(dateFormat.parse(time).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public long timeToDailyLong(String dt) {
        long tmp = 0;
        try {
            tmp = df.parse(dt).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp;
    }

    public long timeLongToHour(long timeLong) {
        long tmp = timeLong % (60 * 60 * 1000);
        return timeLong - tmp;
    }

    public String getDateSql(long dt) {

        Date date = new Date(dt);
        return df.format(date);
    }

    public Long getTime1(Long dt) {
        Date date = new Date(dt);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int minute = c.get(Calendar.MINUTE);
        int min1 = minute - (minute % 1);
        c.set(Calendar.MINUTE, min1);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }
}
