package vn.cmc.global.util;

import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigurationManager {
    private static final String CONFIG_FILE = "config.properties";
    private static String pathConfig = "";
    private Properties pro;
    private static ConfigurationManager conf = new ConfigurationManager();

    static org.apache.logging.log4j.Logger logger = LogManager.getLogger(ConfigurationManager.class);

    private ConfigurationManager() {
        pro = new Properties();
        try {
            loadconfig();
        } catch (Exception e) {
            logger.error("fail to load file config");
        }
    }

    private void loadconfig() throws Exception {
        FileInputStream in = null;
        if (pathConfig == null || pathConfig.isEmpty()) {
            File f = new File(CONFIG_FILE);
            in = new FileInputStream(f);
            pro.load(in);
        } else {
            File f = new File(pathConfig);
            in = new FileInputStream(f);
            pro.load(in);
        }
    }

    public static ConfigurationManager getInstance() {
        return conf;
    }


    public String getString(String name) {
        return pro.getProperty(name);
    }

    public Integer getInt(String name) {
        try {
            String value = getString(name);
            return Integer.parseInt(value);
        } catch (Exception e) {
            return 1;
        }
    }

    public Double getDouble(String name) {
        return Double.valueOf(getString(name));
    }

    public Long getLong(String name) {
        return Long.parseLong(getString(name));
    }

    public Float getFloat(String name) {
        return Float.valueOf(getString(name));
    }
}
