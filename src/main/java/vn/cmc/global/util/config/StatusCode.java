package vn.cmc.global.util.config;

public interface StatusCode {
    int SUCCESS = 0;
    int INVALID_INFORMATION = 1;
    int ERROR = 2;
    int CLIENT_ERROR = 4;
    int SERVER_ERROR = 5;
}
