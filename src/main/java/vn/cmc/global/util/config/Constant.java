package vn.cmc.global.util.config;

/**
 * Created by nhanvt on 2020/06/11
 */
public class Constant {
//    public static int SERVICE_PORT = ConfigurationManager.getInstance().getInt("server.port");

    public static final long EXPIRATION_TIME = 900_000; // 10 days
    public static final String SECRET = "C6860572EC9581630BEFF5142BCEFB9A075A05BB3217BE787085CA8ED6EBF79B";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/services/controller/user";


    //request path
    public static final String AUTH = "/auth";
    public static final String SIGNIN = "/signin";
    public static final String SIGNUP = "/signup";
    public static final String USER = "/user";
    public static final String GET = "/get";
    public static final String CREATE = "/create";
    public static final String EDIT = "/edit";
    public static final String DELETE = "/delete";
    public static final String USER_ID = "/{userId}";
    public static final String ROLE = "/role";

}
