package vn.cmc.global.service;

import vn.cmc.global.entity.ERole;
import vn.cmc.global.entity.Role;

import java.util.Optional;

public interface RoleService {

    Optional<Role> findByName(ERole name);

}
