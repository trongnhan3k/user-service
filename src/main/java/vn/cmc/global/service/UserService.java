package vn.cmc.global.service;

import vn.cmc.global.entity.User;

public interface UserService {

    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    void save(User user);
}