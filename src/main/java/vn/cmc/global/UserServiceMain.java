package vn.cmc.global;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import vn.cmc.global.base.Base;

/**
 * Created by nhanvt on 2020/06/09
 */
@SpringBootApplication
public class UserServiceMain extends Base {

    public static void main(String[] args) {

        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder()
                .sources(UserServiceMain.class)
                .properties();
        applicationBuilder.run(args);

    }
}
